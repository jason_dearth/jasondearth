from pyramid.config import Configurator
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.security import unauthenticated_userid
from sqlalchemy import engine_from_config

from jasondearth.models.modelBase import DBSession, Base
from jasondearth.models.security import User

from jasondearth.models.security import groupfinder


# TODO: this is hitting the database way too much
def get_user(request):
    # check db for user object
    print('***remove this after I figure out how to cache this function***')
    userid = unauthenticated_userid(request)
    if userid is not None:
        return DBSession.query(User).filter(User.username == userid).first()


def format_currency(value):
    return "${:,.2f}".format(value)


def register_routes(config):
    config.add_route('home', '/')
    config.add_route('login', '/login')
    config.add_route('logout', '/logout')
    config.add_route('logged_in', '/loggedin')
    config.add_route('editor', '/editor')
    config.add_route('admin', '/admin')
    config.add_route('register_user', '/register')
    config.add_route('billminder', '/billminder')
    config.add_route('bill', '/bill/{id}')
    config.add_route('bills', '/bill')


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    config = Configurator(settings=settings,
                          root_factory='jasondearth.models.security.Root')

    # security policies
    authn_policy = AuthTktAuthenticationPolicy('sosecret', callback=groupfinder, hashalg='sha512')
    authz_policy = ACLAuthorizationPolicy()
    config.set_authentication_policy(authn_policy)
    config.set_authorization_policy(authz_policy)

    config.include('pyramid_chameleon')
    config.include('pyramid_jinja2')
    config.add_static_view('static', 'static', cache_max_age=3600)
    register_routes(config)
    config.add_request_method(get_user, 'user', reify=True)
    config.scan()
    return config.make_wsgi_app()
