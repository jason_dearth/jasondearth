import datetime
from sqlalchemy import Boolean, Column, Date, Float, ForeignKey, Integer, Text
from jasondearth.models.modelBase import Base, DBSession

dthandler = lambda obj: (
    obj.isoformat()
    if isinstance(obj, datetime.datetime)
    or isinstance(obj, datetime.date)
    else None)


def dump_datetime(value):
    """Deserialize datetime object into string form for JSON processing."""
    if value is None:
        return None
    return value.strftime("%Y-%m-%d")


class Bill(Base):
    __tablename__ = 'bills'
    id = Column(Integer, primary_key=True)
    description = Column(Text, nullable=False)
    username = Column(Text, ForeignKey('users.username'), nullable=False)
    next_due_date = Column(Date)
    status = Column(Text)
    recurring = Column(Boolean)
    last_due_date = Column(Date)
    amount = Column(Float)
    credit = Column(Boolean)

    def __json__(self, request):
        return {'id': self.id,
                'description': self.description,
                'username': self.username,
                'next_due_date': str(dump_datetime(self.next_due_date)),
                'status': self.status,
                'recurring': self.recurring,
                'last_due_date': str(dump_datetime(self.last_due_date)),
                'amount': self.amount,
                'credit': self.credit}

    def to_json(self):
        return {'id': self.id,
                'description': self.description,
                'username': self.username,
                'next_due_date': str(dump_datetime(self.next_due_date)),
                'status': self.status,
                'recurring': self.recurring,
                'last_due_date': str(dump_datetime(self.last_due_date)),
                'amount': self.amount,
                'credit': self.credit}

    @classmethod
    def create_new(cls, username, description, next_due_date, last_due_date,
                   amount, status='due', recurring=False, credit=False):
        bill = Bill(username=username,
                    description=description,
                    next_due_date=next_due_date,
                    status=status,
                    recurring=recurring,
                    last_due_date=last_due_date,
                    amount=amount,
                    credit=credit)
        DBSession.add(bill)
        DBSession.flush()
        return bill

    @classmethod
    def get_bill(cls, id, username):
        try:
            return DBSession.query(Bill).filter(Bill.id == id).filter(Bill.username == username).one()
        except:
            return None

    @classmethod
    def get_by_user(cls, username):
        return DBSession.query(Bill).filter(Bill.username == username).order_by(Bill.next_due_date).all()

    @classmethod
    def delete_by_id(cls, username, bill_id):
        delete_me = DBSession.query(Bill).filter(Bill.id == bill_id).filter(Bill.username == username).first()
        DBSession.delete(delete_me)
