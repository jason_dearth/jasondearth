import bcrypt
from pyramid.security import Allow, ALL_PERMISSIONS, Authenticated
from sqlalchemy import (
    Column,
    Text,
)
from jasondearth.models.modelBase import Base, DBSession


def secure_make_pw_hash(password):
    return bcrypt.hashpw(password, bcrypt.gensalt(12))


def secure_check_pw_hash(password, hashed):
    return bcrypt.checkpw(password, hashed)


USERS = {'editor': 'editor',
         'viewer': 'viewer',
         'admin': 'admin',
         'jasondearth': 'admin'}
GROUPS = {'admin': ['g:admins'],
          'editor': ['g:editors'],
          'viewer': ['g:viewers']}


def groupfinder(userid, request):
    user = DBSession.query(User).filter(User.username == userid).one()
    if user:
        return GROUPS.get(user.sec_group, [])


class User(Base):
    __tablename__ = 'users'
    username = Column(Text, primary_key=True)
    firstname = Column(Text)
    lastname = Column(Text)
    password = Column(Text)
    sec_group = Column(Text)

    @classmethod
    def by_username(cls, username):
        user = cls.query.filter(cls.username == username).first()
        return user

    @classmethod
    def login(cls, username, password):
        user = cls.by_username(username)
        if user and secure_check_pw_hash(password, user.password):
            return user

    @classmethod
    def register(cls, username, password, firstname, lastname, sec_group='viewer'):
        if len(password) < 5:
            return None
        pw_hash = secure_make_pw_hash(password)
        return User(username=username, firstname=firstname, lastname=lastname, password=pw_hash, sec_group=sec_group)


class Root(object):
    __acl__ = [(Allow, Authenticated, 'view'),
               (Allow, 'g:editors', 'edit'),
               (Allow, 'g:admins', ALL_PERMISSIONS)]

    def __init__(self, request):
        pass
