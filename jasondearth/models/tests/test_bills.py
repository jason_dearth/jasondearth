import transaction
import unittest
from pyramid import testing
from jasondearth.tests import init_testing_db


class BillTests(unittest.TestCase):
    def setUp(self):
        self.session = init_testing_db()
        self.config = testing.setUp()
        transaction.begin()
        from jasondearth.models.security import User
        self.username = 'jason'
        firstname = 'Jason'
        lastname = 'Dearth'
        password = 'password'
        user = User.register(username=self.username, firstname=firstname, lastname=lastname, password=password)
        self.session.add(user)

    def tearDown(self):
        transaction.abort()
        self.session.remove()
        testing.tearDown()

    def add_bill(self, description, amount):
        import datetime
        from jasondearth.models.bills import Bill
        d = datetime.date(2014, 7, 1)
        Bill.create_new(username=self.username,
                        description=description,
                        next_due_date=d,
                        last_due_date=d,
                        amount=amount)

    def test_bill_creation(self):
        from jasondearth.models.bills import Bill
        self.add_bill('bill 1', 300.00)
        self.add_bill('bill 2', 400.00)
        bills = Bill.get_by_user(self.username)
        self.assertIsInstance(bills[0], Bill)
        self.assertIsInstance(bills[1], Bill)

    def test_bill_deletion(self):
        from jasondearth.models.bills import Bill
        self.add_bill('delete me', 500.00)
        bills = Bill.get_by_user(self.username)
        self.assertIs(len(bills), 1)
        Bill.delete_by_id(self.username, bills[0].id)
        bills = Bill.get_by_user(self.username)
        self.assertIs(len(bills), 0)

    def test_get_bill_correct_id(self):
        from jasondearth.models.bills import Bill
        self.add_bill('will find', 42)
        msg = Bill.get_bill(1, self.username)
        self.assertIsInstance(msg, Bill)

    def test_get_bill_incorrect_id(self):
        from jasondearth.models.bills import Bill
        msg = Bill.get_bill(100, self.username)
        self.assertIsNone(msg)

    def test_user_can_only_get_bill(self):
        from jasondearth.models.bills import Bill
        self.add_bill('wrong user', 24)
        msg = Bill.get_bill(1, 'wrong')
        self.assertIsNone(msg)

    # def test_json_dump(self):
    #     import json
    #     from jasondearth.models.bills import Bill
    #     self.add_bill('json test', 137)
    #     bill = Bill.get_bill(1, self.username)
    #     # msg = json.dumps(bill)
    #     msg = dict(bill)
    #     self.assertIn('next_due_date', msg)
