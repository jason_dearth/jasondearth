import transaction
import unittest
from pyramid import testing
from jasondearth.tests import init_testing_db


class PasswordHashingTests(unittest.TestCase):
    def test_secure_pw_success(self):
        from jasondearth.models.security import secure_check_pw_hash, secure_make_pw_hash

        password = 'this_is_a_test'
        hashed = secure_make_pw_hash(password)
        self.assertTrue(secure_check_pw_hash(password, hashed))

    def test_secure_pw_failure(self):
        from jasondearth.models.security import secure_check_pw_hash, secure_make_pw_hash

        hashed = secure_make_pw_hash('original')
        self.assertFalse(secure_check_pw_hash('does_not_match', hashed))


class GroupFinderTests(unittest.TestCase):
    def setUp(self):
        self.session = init_testing_db()
        self.config = testing.setUp()
        self.username = 'jasondearth1'
        self.password = '12345'
        self.firstname = 'Jason'
        self.lastname = 'Dearth'
        self.editor = 'editor'
        self.admin = 'admin'

        transaction.begin()

    def tearDown(self):
        transaction.abort()
        self.session.remove()
        testing.tearDown()

    def test_find_existing_viewer(self):
        from jasondearth.models.security import User
        from jasondearth.models.security import groupfinder

        user = User.register(username=self.username,
                             firstname=self.firstname,
                             lastname=self.lastname,
                             password=self.password)
        self.session.add(user)
        result = groupfinder(self.username, 'fake_request')
        self.assertEquals(result, ['g:viewers'])

    def test_find_existing_editor(self):
        from jasondearth.models.security import User
        from jasondearth.models.security import groupfinder

        user = User.register(username=self.username,
                             firstname=self.firstname,
                             lastname=self.lastname,
                             password=self.password,
                             sec_group=self.editor)
        self.session.add(user)
        result = groupfinder(self.username, 'fake_request')
        self.assertEquals(result, ['g:editors'])

    def test_find_existing_admin(self):
        from jasondearth.models.security import User
        from jasondearth.models.security import groupfinder

        user = User.register(username=self.username,
                             firstname=self.firstname,
                             lastname=self.lastname,
                             password=self.password,
                             sec_group=self.admin)
        self.session.add(user)
        result = groupfinder(self.username, 'fake_request')
        self.assertEquals(result, ['g:admins'])


class RegisterUserTests(unittest.TestCase):
    def setUp(self):
        self.session = init_testing_db()
        self.config = testing.setUp()
        self.username = 'jasondearth1'
        self.short = 'shrt'
        self.long = 'long_enough_to_pass'
        self.firstname = 'Jason'
        self.lastname = 'Dearth'

        transaction.begin()

    def tearDown(self):
        transaction.abort()
        self.session.remove()
        testing.tearDown()

    def test_short_password(self):
        from jasondearth.models.security import User
        user = User.register(username=self.username,
                             firstname=self.firstname,
                             lastname=self.lastname,
                             password=self.short)
        self.assertIsNone(user)

    def test_long_enough_password(self):
        from jasondearth.models.security import User
        user = User.register(username=self.username,
                             firstname=self.firstname,
                             lastname=self.lastname,
                             password=self.long)
        self.assertIsInstance(user, User)
