import os
import sys
import transaction

from sqlalchemy import engine_from_config

from pyramid.paster import (
    get_appsettings,
    setup_logging,
    )

from pyramid.scripts.common import parse_vars

from jasondearth.models.modelBase import DBSession, Base
from jasondearth.models.security import User
from jasondearth.models.bills import Bill


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri> [var=value]\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)


def main(argv=sys.argv):
    if len(argv) < 2:
        usage(argv)
    config_uri = argv[1]
    options = parse_vars(argv[2:])
    setup_logging(config_uri)
    settings = get_appsettings(config_uri, options=options)
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.create_all(engine)
    with transaction.manager:
        # create my first admin user
        username = 'jasondearth'
        password = '12345'
        user = User.register(username=username, firstname='Jason', lastname='Dearth', password=password, sec_group='admin')
        DBSession.add(user)

        import datetime
        d = datetime.date(2014, 7, 1)
        bill = Bill(username=username, description='bill 1', next_due_date=d, amount=100.00)
        DBSession.add(bill)
        d = datetime.date(2014, 7, 15)
        bill = Bill(username=username, description='bill 2', next_due_date=d, amount=300.00)
        DBSession.add(bill)

        # create my first editor user
        username = 'editor'
        password = 'editor'
        user = User.register(username=username, firstname='Mr.', lastname='Editor', password=password, sec_group='editor')
        DBSession.add(user)

        # create my first viewer user
        username = 'viewer'
        password = 'viewer'
        user = User.register(username=username, firstname='Ms.', lastname='Viewer', password=password)
        DBSession.add(user)
