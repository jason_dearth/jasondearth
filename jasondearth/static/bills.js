angular.module('billApp', ['ngRoute', 'billApp.services', 'billApp.controllers']).
    config(function ($routeProvider, $httpProvider) {
        $routeProvider.when('/bill-list', {templateUrl: 'static/bills/bill-list.html', controller: 'BillListCtrl'});
        $routeProvider.otherwise({redirectTo: '/bill-list'});
    });