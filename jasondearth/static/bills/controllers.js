var app = angular.module('billApp.controllers', []);

app.controller('BillListCtrl', ['$scope', 'billsFactory', 'billFactory', '$location',
    function ($scope, billsFactory, billFactory, $location) {
        $scope.datePattern=/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/i;

        $scope.sortByKey = function (array, key) {
            return array.sort(function(a, b) {
                var x = a[key];
                var y = b[key];
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            });
        }

        $scope.getRunningTotal = function (idx) {
            var runningTotal = 0;
            var selectedBills = $scope.bills.slice(0, idx + 1);
            angular.forEach(selectedBills, function (bill, idx) {
                runningTotal += bill.amount;
            });
            return runningTotal;
        };

        $scope.addBill = function () {
            billsFactory.create($scope.bill).then(function(data){
                $scope.bill.description = "";
                $scope.bill.next_due_date = "";
                $scope.bill.amount = "";
                $scope.addBillForm.$setPristine();
                $scope.bills.push(data.data);
                $scope.bills = $scope.sortByKey($scope.bills, 'next_due_date');
            });
        };

        $scope.deleteBill = function (delBill) {
            var idx = $scope.bills.indexOf(delBill);
            billFactory.delete(delBill.id).then(function() {
                $scope.bills.splice(idx, 1);
            });
        };

        $scope.editBill = function (bill) {
            bill.edit = true;
        };

        $scope.saveBill = function (bill) {
            var idx = $scope.bills.indexOf(bill);
            billFactory.put(bill.id, angular.toJson(bill)).then(function() {
                bill.edit = false;
                $scope.bills = $scope.sortByKey($scope.bills, 'next_due_date');
            });
            //bill.edit = false;
        };

        $scope.submitPayment = function (bill) {
            var idx = $scope.bills.indexOf(bill);
            billFactory.put(bill.id, angular.toJson({'status': 'paid'})).then(function() {
                $scope.bills[idx].status = 'paid';
            });
        };

        billsFactory.query().success(function(bills){
            $scope.bills = bills;
        });
    }]);