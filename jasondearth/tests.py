import unittest
import transaction
from pyramid import testing
from jasondearth import register_routes


class Jinja2FilterTests(unittest.TestCase):
    def test_currency_format(self):
        from jasondearth import format_currency
        currency = format_currency(1000000.00)
        self.assertEqual(currency, '$1,000,000.00')


def init_testing_db():
    from sqlalchemy import create_engine
    from jasondearth.models.modelBase import (
        DBSession,
        Base,
    )

    engine = create_engine('sqlite://')
    Base.metadata.create_all(engine)
    DBSession.configure(bind=engine)
    return DBSession


class ViewHomeTests(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()

    def _call_fut(self, request):
        from jasondearth.views.auth_views import my_view

        return my_view(request)

    def test_it(self):
        register_routes(self.config)
        request = testing.DummyRequest()
        response = self._call_fut(request)
        self.assertEqual(response['project'], 'jasondearth')


class FunctionalTests(unittest.TestCase):
    def setUp(self):
        from jasondearth import main

        settings = {'sqlalchemy.url': 'sqlite://'}
        app = main({}, **settings)
        from webtest import TestApp

        self.testapp = TestApp(app)
        init_testing_db()

    def tearDown(self):
        del self.testapp
        from jasondearth.models.modelBase import DBSession

        DBSession.remove()

    def test_root(self):
        res = self.testapp.get('/', status=200)
        self.assertTrue(b'Pyramid' in res.body)

    def test_missing_page(self):
        self.testapp.get('/SomePage', status=404)
