import re
from pyramid.response import Response
from pyramid.view import view_config, forbidden_view_config
from pyramid.security import (
    remember,
    forget,
)
from pyramid.httpexceptions import (
    HTTPFound,
)
from jasondearth.models.modelBase import DBSession
from jasondearth.models.security import User


USER_RE = re.compile(r'^[a-zA-Z0-9_-]{3,20}$')
NAME_RE = re.compile(r'^[a-zA-Z]{2,20}$')
PASSWORD_RE = re.compile(r'^.{3,20}$')


def user_already_exists(username):
    return DBSession.query(User).filter(User.username == username).first()


def valid_username(username):
    return USER_RE.match(username)


def valid_name(name):
    return NAME_RE.match(name)


def valid_password(password):
    return PASSWORD_RE.match(password)


@forbidden_view_config()
def forbidden(request):
    return Response('forbidden')


@view_config(route_name='logged_in', permission='view', renderer='templates/logged_in.jinja2')
def logged_in(request):
    return {}


@view_config(route_name='login', renderer='templates/login.jinja2')
def login(request):
    have_error = False
    errors = []
    login_url = request.route_url('login')
    referrer = request.url
    if referrer == login_url:
        referrer = '/'  # never use the login form itself as came_from
    came_from = request.params.get('came_from', referrer)
    username = ''
    password = ''
    if 'form.submitted' in request.params:
        username = request.params['username']
        password = request.params['password']

        if not valid_username(username):
            errors.append('Invalid username')
            have_error = True

        if not valid_password(password):
            errors.append('Invalid password')
            have_error = True

        if not have_error:
            valid_user = User.login(username, password)
            if valid_user:
                headers = remember(request, username)
                return HTTPFound(location=request.route_url('logged_in'), headers=headers)
            else:
                errors.append('User not found or invalid password')

    return dict(
        errors=errors,
        url=request.application_url + '/login',
        came_from=came_from,
        username=username,
        password=password,
    )


@view_config(route_name='register_user', renderer='templates/register_user.jinja2')
def register_user(request):
    have_error = False
    errors = []
    username = ''
    firstname = ''
    lastname = ''
    if 'form.submitted' in request.params:
        username = request.params['username']
        password = request.params['password']
        verify_password = request.params['verify_password']
        firstname = request.params['firstname']
        lastname = request.params['lastname']

        if not valid_username(username):
            errors.append('Invalid username')
            have_error = True

        if user_already_exists(username):
            errors.append('User already exists')
            have_error = True

        if not valid_name(firstname):
            errors.append('Invalid first name')
            have_error = True

        if not valid_name(lastname):
            errors.append('Invalid last name')
            have_error = True

        if not valid_password(password):
            errors.append('Invalid password')
            have_error = True

        if password != verify_password:
            errors.append('Passwords did not match')
            have_error = True

        if not have_error:
            user = User.register(username=username, firstname=firstname, lastname=lastname, password=password)
            if user:
                DBSession.add(user)
                headers = remember(request, username)
                return HTTPFound(location=request.route_url('logged_in'), headers=headers)
            else:
                errors.append('There was an unknown error')

    return dict(
        errors=errors,
        username=username,
        firstname=firstname,
        lastname=lastname,
    )


@view_config(route_name='logout')
def logout(request):
    headers = forget(request)
    return HTTPFound(location=request.route_url('home'), headers=headers)


@view_config(route_name='home', renderer='templates/home.jinja2')
def my_view(request):
    return {'one': 'one', 'project': 'jasondearth'}


@view_config(route_name='editor', permission='edit', renderer='templates/logged_in.jinja2')
def editor_view(request):
    return {}


@view_config(route_name='admin', permission='admin', renderer='templates/logged_in.jinja2')
def admin_view(request):
    return {}
