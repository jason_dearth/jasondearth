import json
import numbers
from datetime import datetime
from pyramid.response import Response
from pyramid.view import view_config, view_defaults
from jasondearth.models.bills import Bill


@view_config(route_name='billminder', permission='view', renderer='templates/bills.jinja2')
def bill_minder(request):
    #bills = Bill.get_by_user(request.user.username)
    return {} #{'bills': bills}


@view_defaults(route_name='bill', permission='view', renderer='json')
class BillView(object):
    def __init__(self, request):
        self.request = request

    @view_config(route_name='bills', request_method='POST', renderer='json')
    def add_bill(self):
        have_error = False
        #if 'form.submitted' in self.request.params:
        request_params = self.request.json_body
        description = request_params['description']
        due_date = request_params['next_due_date']
        try:
            date_object = datetime.strptime(due_date, '%Y-%m-%d')
        except ValueError:
            have_error = True
        amount = request_params['amount']

        #TODO: insert validations here
        if len(description) == 0:
            have_error = True

        if not isinstance(amount, numbers.Number):
            have_error = True

        if not have_error:
            bill = Bill.create_new(username=self.request.user.username,
                                   description=description,
                                   next_due_date=date_object,
                                   last_due_date=date_object,
                                   amount=amount)
            return bill.to_json()
        return Response(status='400 Bad Request')

    @view_config(request_method='DELETE')
    def delete_bill(self):
        id = self.request.matchdict['id']
        Bill.delete_by_id(self.request.user.username, id)
        return Response(status='200', content_type='application/json; charset=UTF-8')

    @view_config(request_method='GET')
    def get_bill(self):
        id = self.request.matchdict['id']
        bill = Bill.get_bill(id, self.request.user.username)
        if bill:
            return {'bill': bill}
        return Response(status='404 Not Found', content_type='application/json; charset=UTF-8')

    @view_config(request_method='PUT')
    def update_bill(self):
        id = self.request.matchdict['id']
        request_params = self.request.json_body
        bill = Bill.get_bill(id, self.request.user.username)
        if bill:
            if 'status' in request_params:
                bill.status = request_params['status']
            if 'description' in request_params:
                bill.description = request_params['description']
            if 'amount' in request_params:
                bill.amount = request_params['amount']
            if 'next_due_date' in request_params:
                try:
                    bill.next_due_date = datetime.strptime(request_params['next_due_date'], '%Y-%m-%d')
                except ValueError:
                    return Response(status='400')
            if 'last_due_date' in request_params:
                try:
                    bill.last_due_date = datetime.strptime(request_params['last_due_date'], '%Y-%m-%d')
                except ValueError:
                    return Response(status='400')
            return Response(status='200')
        return Response(status='400 Bad Request')

    @view_config(route_name='bills', request_method='GET')
    def get_bills(self):
        bills = Bill.get_by_user(self.request.user.username)
        return bills
