import unittest
import transaction
from pyramid import testing
from jasondearth import register_routes
from jasondearth.tests import init_testing_db


class UsernameLengthTests(unittest.TestCase):
    def test_successful_username(self):
        from jasondearth.views.auth_views import valid_username
        result = valid_username('jasondearth1')
        self.assertTrue(result)

    def test_too_short(self):
        from jasondearth.views.auth_views import valid_username
        result = valid_username('me')
        self.assertFalse(result)

    def test_too_long(self):
        from jasondearth.views.auth_views import valid_username
        result = valid_username('jasondearthjasondearthjasondearth')
        self.assertFalse(result)

    def test_illegal_character(self):
        from jasondearth.views.auth_views import valid_username
        result = valid_username('jason#dearth')
        self.assertFalse(result)


class NameTests(unittest.TestCase):
    def test_successful_name(self):
        from jasondearth.views.auth_views import valid_name
        result = valid_name('Jason')
        self.assertTrue(result)

    def test_too_short(self):
        from jasondearth.views.auth_views import valid_name
        result = valid_name('a')
        self.assertFalse(result)

    def test_too_long(self):
        from jasondearth.views.auth_views import valid_name
        result = valid_name('jasondearthjasondearthjasondearth')
        self.assertFalse(result)

    def test_illegal_character(self):
        from jasondearth.views.auth_views import valid_name
        result = valid_name('Jason&')
        self.assertFalse(result)


class PasswordTests(unittest.TestCase):
    def test_successful_password(self):
        from jasondearth.views.auth_views import valid_password
        result = valid_password('*tH9_f4@')
        self.assertTrue(result)

    def test_too_short(self):
        from jasondearth.views.auth_views import valid_password
        result = valid_password('m9')
        self.assertFalse(result)

    def test_too_long(self):
        from jasondearth.views.auth_views import valid_password
        result = valid_password('this_is*too@LONG29255Test')
        self.assertFalse(result)


class ViewLoginLogoutTests(unittest.TestCase):
    def setUp(self):
        self.session = init_testing_db()
        self.config = testing.setUp()
        register_routes(self.config)
        self.username = 'jasondearth1'
        self.password = '12345'
        self.firstname = 'Jason'
        self.lastname = 'Dearth'
        from jasondearth.models.security import User

        transaction.begin()
        user = User.register(username=self.username,
                             firstname=self.firstname,
                             lastname=self.lastname,
                             password=self.password
        )
        self.session.add(user)

    def tearDown(self):
        transaction.abort()
        self.session.remove()
        testing.tearDown()

    def _call_login(self, request):
        from jasondearth.views.auth_views import login

        return login(request)

    def _call_logout(self, request):
        from jasondearth.views.auth_views import logout

        return logout(request)

    def test_success_login(self):
        request = testing.DummyRequest(
            params={'username': self.username, 'password': self.password, 'form.submitted': '1', })
        response = self._call_login(request)
        self.assertEqual(response.status, '302 Found')
        self.assertEqual(response.location, request.route_url('logged_in'))

    def test_success_logout(self):
        from pyramid.security import remember

        request1 = testing.DummyRequest()
        headers = remember(request1, 'cookie_test')
        request = testing.DummyRequest(headers=headers)
        response = self._call_logout(request)
        self.assertNotEqual(response.headers, headers)

    def test_wrong_password(self):
        request = testing.DummyRequest(params={'username': self.username, 'password': 'wrong', 'form.submitted': '1', })
        response = self._call_login(request)
        self.assertTrue(response.get('errors'))

    def test_missing_username_password(self):
        request = testing.DummyRequest(params={'username': '', 'password': '', 'form.submitted': '1', })
        response = self._call_login(request)
        self.assertIn('Invalid username', response.get('errors'))
        self.assertIn('Invalid password', response.get('errors'))

    def test_short_username_password(self):
        request = testing.DummyRequest(params={'username': 'sh', 'password': '12', 'form.submitted': '1', })
        response = self._call_login(request)
        self.assertIn('Invalid username', response.get('errors'))
        self.assertIn('Invalid password', response.get('errors'))

    def test_long_username_password(self):
        request = testing.DummyRequest(params={'username': 'reallylongusernametoo',
                                               'password': 'reallylongpasswordtoo',
                                               'form.submitted': '1', })
        response = self._call_login(request)
        self.assertIn('Invalid username', response.get('errors'))
        self.assertIn('Invalid password', response.get('errors'))

    def test_home_login(self):  # this proves that my testing is starting with a fresh database instance
        username = 'jasondearth'
        password = '12345'
        request = testing.DummyRequest(params={'username': username, 'password': password, 'form.submitted': '1', })
        response = self._call_login(request)
        self.assertTrue(response.get('errors'))


class ViewRegisterUserTests(unittest.TestCase):
    def setUp(self):
        self.session = init_testing_db()
        self.config = testing.setUp()
        register_routes(self.config)
        transaction.begin()

    def tearDown(self):
        transaction.abort()
        self.session.remove()
        testing.tearDown()

    def _call_register(self, request):
        from jasondearth.views.auth_views import register_user
        return register_user(request)

    def test_missing_fields(self):
        request = testing.DummyRequest(params={'username': '',
                                               'firstname': '',
                                               'lastname': '',
                                               'password': '',
                                               'verify_password': '',
                                               'form.submitted': '1', })
        response = self._call_register(request)
        self.assertIn('Invalid username', response.get('errors'))
        self.assertIn('Invalid password', response.get('errors'))
        self.assertIn('Invalid first name', response.get('errors'))
        self.assertIn('Invalid last name', response.get('errors'))

    def test_passwords_not_matching(self):
        request = testing.DummyRequest(params={'username': 'passwordval',
                                               'firstname': 'pass',
                                               'lastname': 'word',
                                               'password': 'password',
                                               'verify_password': 'incorrect',
                                               'form.submitted': '1', })
        response = self._call_register(request)
        self.assertIn('Passwords did not match', response.get('errors'))

    def test_successful_registration(self):
        request = testing.DummyRequest(params={'username': 'newuser',
                                               'firstname': 'first',
                                               'lastname': 'last',
                                               'password': 'password',
                                               'verify_password': 'password',
                                               'form.submitted': '1', })
        response = self._call_register(request)
        self.assertEqual(response.status, '302 Found')
        self.assertEqual(response.location, request.route_url('logged_in'))

    def test_duplicate_username(self):
        request = testing.DummyRequest(params={'username': 'newuser',
                                               'firstname': 'first',
                                               'lastname': 'last',
                                               'password': 'password',
                                               'verify_password': 'password',
                                               'form.submitted': '1', })
        first_user = self._call_register(request)
        duplicate = self._call_register(request)
        self.assertIn('User already exists', duplicate.get('errors'))
