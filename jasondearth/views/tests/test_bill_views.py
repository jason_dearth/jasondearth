import unittest
import transaction
from pyramid import testing
from pyramid.response import Response
from jasondearth import register_routes
from jasondearth.tests import init_testing_db


class ViewBillTests(unittest.TestCase):
    def setUp(self):
        self.session = init_testing_db()
        self.config = testing.setUp()
        register_routes(self.config)
        self.username = 'jasondearth1'
        self.password = '12345'
        self.firstname = 'Jason'
        self.lastname = 'Dearth'
        from jasondearth.models.security import User

        transaction.begin()
        user = User.register(username=self.username,
                             firstname=self.firstname,
                             lastname=self.lastname,
                             password=self.password
        )
        self.session.add(user)
        from pyramid.security import remember
        request = testing.DummyRequest()
        self.headers = remember(request, self.username)
        self.user = self.session.query(User).filter(User.username == self.username).first()

    def tearDown(self):
        transaction.abort()
        self.session.remove()
        testing.tearDown()

    def _call_bill_minder(self, request):
        from jasondearth.views.bills_views import bill_minder
        return bill_minder(request)

    def _call_add_bill(self, description='', due_date='', amount=''):
        from jasondearth.views.bills_views import BillView
        json_body = {'description': description,
                     'next_due_date': due_date,
                     'amount': amount}
        request = testing.DummyRequest(json_body=json_body,
                                       headers=self.headers,
                                       user=self.user)
        bill_object = BillView(request)
        return bill_object.add_bill()

    def _call_delete_bill(self, id):
        from jasondearth.views.bills_views import BillView
        request = testing.DummyRequest(headers=self.headers, user=self.user)
        request.matchdict['id'] = id
        bill_object = BillView(request)
        return bill_object.delete_bill()

    def _call_get_bill(self, id):
        from jasondearth.views.bills_views import BillView
        request = testing.DummyRequest(headers=self.headers, user=self.user)
        request.matchdict['id'] = id
        bill_object = BillView(request)
        return bill_object.get_bill()

    def _call_get_bills(self):
        from jasondearth.views.bills_views import BillView
        request = testing.DummyRequest(headers=self.headers, user=self.user)
        bill_object = BillView(request)
        return bill_object.get_bills()

    def test_bill_minder_empty(self):
        request = testing.DummyRequest(headers=self.headers, user=self.user)
        response = self._call_bill_minder(request)
        self.assertEqual(response, {})

    def test_bill_minder_return_results(self):
        description1 = 'first'
        due_date1 = '2014-12-12'
        amount1 = 100.00
        self._call_add_bill(description=description1, due_date=due_date1, amount=amount1)

        description2 = 'second'
        due_date2 = '2014-12-26'
        amount2 = 300.00
        self._call_add_bill(description=description2, due_date=due_date2, amount=amount2)

        response = self._call_get_bills()
        self.assertIs(len(response), 2)

    def test_add_bill_missing_description(self):
        due_date = '2014-07-01'
        amount = 100.00
        response = self._call_add_bill(due_date=due_date, amount=amount)
        self.assertEqual(response._status, '400 Bad Request')

    def test_add_bill_missing_amount(self):
        description = 'missing amount'
        due_date = '2014-07-01'
        response = self._call_add_bill(description=description, due_date=due_date)
        self.assertEqual(response._status, '400 Bad Request')

    def test_add_bill_amount_nan(self):
        description = 'not numeric amount'
        due_date = '2014-07-01'
        amount = 'test'
        response = self._call_add_bill(description=description, due_date=due_date, amount=amount)
        self.assertEqual(response._status, '400 Bad Request')

    def test_add_bill_not_date(self):
        description = 'not a date'
        due_date = 'date'
        amount = 100.00
        response = self._call_add_bill(description=description, due_date=due_date, amount=amount)
        self.assertEqual(response._status, '400 Bad Request')

    def test_add_bill_correct(self):
        from jasondearth.models.bills import Bill
        description = 'this works'
        due_date = '2014-07-01'
        amount = 100.00
        response = self._call_add_bill(description=description, due_date=due_date, amount=amount)
        self.assertEqual(response['description'], 'this works')

    def test_delete_bill(self):
        description = 'delete me'
        due_date = '2014-01-01'
        amount = 1
        self._call_add_bill(description=description, due_date=due_date, amount=amount)

        response = self._call_get_bills()
        bill = response[0]
        deleted = self._call_delete_bill(bill.id)
        self.assertEqual(deleted._status, '200 OK')

    def test_get_bill(self):
        description = 'catch me if you can'
        due_date = '2014-01-01'
        amount = 50
        self._call_add_bill(description=description, due_date=due_date, amount=amount)

        all_bills = self._call_get_bills()
        bill = all_bills[0]
        response = self._call_get_bill(bill.id)
        self.assertEqual((response['bill']).description, 'catch me if you can')

    def test_get_bill_missing(self):
        response = self._call_get_bill(1)
        self.assertEqual(response._status, '404 Not Found')
